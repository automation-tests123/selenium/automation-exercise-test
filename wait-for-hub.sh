#!/bin/bash

echo "Waiting for Selenium Hub to become available..."
while ! curl --output /dev/null --silent --head --fail "http://selenium-hub:4444/wd/hub/status"; do
  echo -n '.'
  sleep 5
done
echo "Selenium Hub is up and running."
