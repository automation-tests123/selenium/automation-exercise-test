#!/bin/bash

# Define the number of runs
count=3

# Create a directory for Extent Reports if it doesn't exist
mkdir -p extent-reports

# Loop to execute the tests X times
for i in $(seq 1 $count); do
    echo "Execution $i of $count"

    # Running the tests with the provided parameters and capturing the output
    mvn_output=$(mvn test -DsuiteXmlFile=all_tests_suite.xml 2>&1)
    echo "$mvn_output"

    # Define the location of the Extent Reports
    report_location="extent-reports/extent-report.html"

    # Check if the report exists
    if [ -f "$report_location" ]; then
        # Creating a unique name for the folder for this execution
        timestamp=$(date +%H#%M#%S)
        report_name="report_${i}-${timestamp}"
        report_directory="extent-reports/$report_name"
        mkdir -p "$report_directory"

        # Moving the generated report to the created folder
        mv "$report_location" "$report_directory/"
        echo "Report saved in $report_directory"
    else
        echo "No Extent Report generated for execution $i"
    fi

    # Recording the execution status
    if [[ $mvn_output == *"BUILD SUCCESS"* ]]; then
        echo "Execution $i: Success" >> "extent-reports/run_status.txt"
    elif [[ $mvn_output == *"Failures:"* ]]; then
        echo "Execution $i: Failure" >> "extent-reports/run_status.txt"
    else
        echo "Execution $i: Unknown result" >> "extent-reports/run_status.txt"
    fi
done

echo "All executions complete. Check the 'extent-reports' directory for details."