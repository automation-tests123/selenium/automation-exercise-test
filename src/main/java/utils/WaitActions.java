package utils;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.time.Duration;

public class WaitActions {
    private static final int DEFAULT_WAIT_TIME_IN_SECONDS = 10;
    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final int SLEEP_INTERVAL_IN_MILLISECONDS = 500;
    private static final WebDriverWait wait =
            new WebDriverWait(TestSetup.getDriverThreadLocal(), Duration.ofSeconds(DEFAULT_WAIT_TIME_IN_SECONDS));

    public static void clickElement(WebElement webElement) {
        wait.until(ExpectedConditions.elementToBeClickable(webElement)).click();
    }

    public static void typeIntoElement(WebElement webElement, String text) {
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
        webElement.sendKeys(text);
    }

    public void waitForElementAppear(WebElement webElement) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public boolean waitForFile(String filePath, int timeoutInSeconds) throws InterruptedException {
        File file = new File(filePath);
        int timeElapsed = 0;
        int timeoutInMilliseconds = timeoutInSeconds * MILLISECONDS_PER_SECOND;
        while (timeElapsed < timeoutInMilliseconds) {
            if (file.exists()) {
                return true;
            }
            Thread.sleep(SLEEP_INTERVAL_IN_MILLISECONDS);
            timeElapsed += SLEEP_INTERVAL_IN_MILLISECONDS;
        }
        return file.exists();
    }
}
