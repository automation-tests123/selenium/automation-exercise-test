package utils;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utils.extentreports.ExtentTestManager;

import static org.assertj.core.api.Assertions.assertThat;

public class AppMainMethods {

    public void clearCookiesAndRefresh() {
        ExtentTestManager.getTest().log(Status.INFO, "Delete cookies and refresh view");
        TestSetup.getDriverThreadLocal().manage().deleteAllCookies();

        TestSetup.getDriverThreadLocal().navigate().refresh();
    }

    public AppMainMethods verifyThatHomePageIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that home page is visible successfully");
        assertThat(TestSetup.getDriverThreadLocal().getCurrentUrl()).isEqualTo("https://automationexercise.com/");
        return this;
    }

    public void verifyThatLoginPageIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that login page is visible successfully");
        assertThat(TestSetup.getDriverThreadLocal().getCurrentUrl()).isEqualTo("https://automationexercise.com/login");
    }

    public void scrollToTheBottomOfThePage() {
        ExtentTestManager.getTest().log(Status.INFO, "Scroll to the bottom of the page");
        ((JavascriptExecutor) TestSetup.getDriverThreadLocal())
                .executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    public void scrollToTheTopOfThePage() {
        ExtentTestManager.getTest().log(Status.INFO, "Scroll to the top of the page");
        ((JavascriptExecutor) TestSetup.getDriverThreadLocal()).executeScript("window.scrollTo(0, 0)");
    }

    public void hoverOverElement(WebElement element) {
        ExtentTestManager.getTest().log(Status.INFO, "Hover to the element");
        Actions action = new Actions(TestSetup.getDriverThreadLocal());
        action.moveToElement(element).perform();
    }

    public void scrollToElement(WebElement element) {
        ExtentTestManager.getTest().log(Status.INFO, "Scroll to the element");
        ((JavascriptExecutor) TestSetup.getDriverThreadLocal())
                .executeScript("arguments[0].scrollIntoView(true);", element);
    }
}
