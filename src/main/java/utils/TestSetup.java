package utils;

import lombok.Getter;
import lombok.Setter;
import navigation.AppURLs;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.logs.Log;

import java.io.File;
import java.lang.reflect.Method;
import java.util.HashMap;

import static utils.extentreports.ExtentTestManager.startTest;

public class TestSetup {

    protected static ThreadLocal<ChromeDriver> driverThreadLocal = new ThreadLocal<>();
    @Getter
    @Setter
    private static WaitActions waitActions;

    public static ChromeDriver getDriverThreadLocal() {
        return driverThreadLocal.get();
    }

    @BeforeMethod
    public void setup(Method method) {
        Log.info("Tests are starting!");
        Log.info("Test is running on Chrome");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addExtensions(new File("src/test/resources/adblock/Adblock.crx"));
        if (Boolean.parseBoolean(System.getProperty("isHeadless"))) {
            chromeOptions.addArguments("--headless=new");
        }
        chromeOptions.addArguments("--no-sandbox", "--disable-dev-shm-usage", "--disable-search-engine-choice-screen");

        HashMap<String, Object> chromePrefs = new HashMap<>();
        String chromeDownloadFilepath = new File("src/test/resources/downloaded-file").getAbsolutePath();
        chromePrefs.put("download.default_directory", chromeDownloadFilepath);
        chromeOptions.setExperimentalOption("prefs", chromePrefs);

        driverThreadLocal.set(new ChromeDriver(chromeOptions));
        driverThreadLocal.get().manage().window().maximize();
        driverThreadLocal.get().navigate().to(AppURLs.APP_URL);

        Cookie consentCookie = new Cookie(
                "cookie_consent",
                "true",
                ".automationexercise.com",
                "/",
                null
        );
        driverThreadLocal.get().manage().addCookie(consentCookie);
        driverThreadLocal.get().navigate().refresh();
        System.out.println("Consent cookie added successfully.");

        setWaitActions(new WaitActions());

        String testName = method.getName();
        Test testAnnotation = method.getAnnotation(Test.class);
        String description = testAnnotation.description();

        startTest(testName, description);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        ChromeDriver driver = driverThreadLocal.get();
        Log.info("Tests are ending!");
        if (driver != null) {
            try {
                driver.quit();
            } catch (Exception e) {
                // Ignore exceptions during quit
            } finally {
                driverThreadLocal.remove();
            }
        }
    }
}