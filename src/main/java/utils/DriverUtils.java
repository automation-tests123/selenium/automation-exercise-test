package utils;

public class DriverUtils {

    public static void setInitialConfiguration() {
        TestSetup.getDriverThreadLocal().manage().window().maximize();
    }

    public static void navigateTo(String url) {
        TestSetup.getDriverThreadLocal().navigate().to(url);
    }
}
