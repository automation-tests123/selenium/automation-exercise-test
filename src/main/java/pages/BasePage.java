package pages;

import org.openqa.selenium.support.PageFactory;
import utils.TestSetup;

public class BasePage extends TestSetup {

    public BasePage() {
        PageFactory.initElements(TestSetup.getDriverThreadLocal(), this);
    }
}
