package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WaitActions;
import utils.extentreports.ExtentTestManager;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginPage extends BasePage {

    @FindBy(css = ".login-form h2")
    private WebElement loginHeaderTextBeforeLogin;

    @FindBy(css = "input[data-qa='login-email']")
    private WebElement loginEmailInput;

    @FindBy(css = "input[data-qa='login-password']")
    private WebElement loginPasswordInput;

    @FindBy(css = "button[data-qa='login-button']")
    private WebElement loginButton;

    @FindBy(css = ".nav li:nth-child(10) a")
    private WebElement loggedUsernameText;

    @FindBy(css = ".login-form p")
    private WebElement loginErrorInformationText;

    public LoginPage() {
        super();
    }

    public LoginPage verifyHeaderBeforeLoginIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify 'Login to your account' is visible");
        assertThat(loginHeaderTextBeforeLogin.getText()).isEqualTo("Login to your account");

        return this;
    }

    public LoginPage setLoginEmailInput(String emailInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting login email input");
        WaitActions.typeIntoElement(loginEmailInput, emailInput);

        return this;
    }

    public LoginPage setLoginPasswordInput(String passwordInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting login password input");
        WaitActions.typeIntoElement(loginPasswordInput, passwordInput);

        return this;
    }

    public LoginPage clickLoginButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the login button");
        WaitActions.clickElement(loginButton);

        return this;
    }

    public void verifyLoggedUsernameIsVisibleSuccessfully(String loggedUsername) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that 'Logged in as username' is visible");
        assertThat(loggedUsernameText.getText()).isEqualTo("Logged in as " + loggedUsername);
    }

    public void verifyErrorForIncorrectCredentialsIsVisibleSuccessfully() {
        ExtentTestManager.getTest()
                .log(Status.INFO, "Verify error 'Your email or password is incorrect!' is visible");
        assertThat(loginErrorInformationText.getText()).isEqualTo("Your email or password is incorrect!");
    }
}