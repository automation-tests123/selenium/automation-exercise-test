package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import utils.WaitActions;
import utils.extentreports.ExtentTestManager;

import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class SignupPage extends BasePage {

    @FindBy(css = ".signup-form h2")
    private WebElement signupHeaderTextBeforeRegistartion;

    @FindBy(css = "input[data-qa='signup-name']")
    private WebElement signupNameInput;

    @FindBy(css = "input[data-qa='signup-email']")
    private WebElement signupEmailInput;

    @FindBy(css = "button[data-qa='signup-button']")
    private WebElement signupButton;

    @FindBy(css = "h2.title")
    private WebElement signupHeaderTextDuringRegistartion;

    @FindBy(css = "div[data-qa='title']")
    private WebElement signupTitleCheckbox;

    @FindBy(css = "input[data-qa='password']")
    private WebElement signupPasswordInput;

    @FindBy(css = "select[data-qa='days']")
    private WebElement signupDaysSelect;

    @FindBy(css = "select[data-qa='months']")
    private WebElement signupMonthsSelect;

    @FindBy(css = "select[data-qa='years']")
    private WebElement signupYearsSelect;

    @FindBy(css = "label[for='newsletter']")
    private WebElement signupNewsletterCheckbox;

    @FindBy(css = "label[for='optin']")
    private WebElement signupSpecialOffersCheckbox;

    @FindBy(css = "input[data-qa='first_name']")
    private WebElement signupFirstnameInput;

    @FindBy(css = "input[data-qa='last_name']")
    private WebElement signupLastnameInput;

    @FindBy(css = "input[data-qa='company']")
    private WebElement signupCompanyInput;

    @FindBy(css = "input[data-qa='address']")
    private WebElement signupFirstAddressInput;

    @FindBy(css = "input[data-qa='address2']")
    private WebElement signupSecondAddressInput;

    @FindBy(css = "select[data-qa='country']")
    private WebElement signupCountrySelect;

    @FindBy(css = "input[data-qa='state']")
    private WebElement signupStateInput;

    @FindBy(css = "input[data-qa='city']")
    private WebElement signupCityInput;

    @FindBy(css = "input[data-qa='zipcode']")
    private WebElement signupZipcodeInput;

    @FindBy(css = "input[data-qa='mobile_number']")
    private WebElement signupMobileNumberInput;

    @FindBy(css = "button[data-qa='create-account']")
    private WebElement signupCreateAccountButton;

    @FindBy(css = "h2[data-qa='account-created']")
    private WebElement signupHeaderTextAfterRegistartion;

    @FindBy(css = "a[data-qa='continue-button']")
    private WebElement signupContinueButton;

    @FindBy(css = "li:nth-child(10) a")
    private WebElement signupLoggedInTextUsernameLink;

    @FindBy(css = "div[id='dismiss-button']")
    private WebElement signupCloseAdsGoogleButton;

    @FindBy(css = "iframe[name='aswift_1']")
    private List<WebElement> signupGoogleAdsFirstIframe;

    @FindBy(css = "iframe[name='ad_iframe']")
    private List<WebElement> signupGoogleAdsSecondIframe;

    @FindBy(css = ".signup-form p")
    private WebElement signupErrorInformationEmailAlreadyText;

    public SignupPage() {
        super();
    }

    private void chooseRandomValueInSelect(WebElement selector) {
        Select select = new Select(selector);

        List<WebElement> options = select.getOptions();

        List<WebElement> nonEmptyOptions = options.stream()
                .filter(option -> !option.getAttribute("value").isEmpty())
                .toList();

        Random rand = new Random();
        WebElement randomOption = nonEmptyOptions.get(rand.nextInt(nonEmptyOptions.size()));

        select.selectByValue(randomOption.getAttribute("value"));
    }

    public SignupPage setSignupNameInput(String nameInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting signup name input");
        WaitActions.typeIntoElement(signupNameInput, nameInput);

        return this;
    }

    public SignupPage setSignupEmailInput(String emailInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting signup email input");
        WaitActions.typeIntoElement(signupEmailInput, emailInput);

        return this;
    }

    public SignupPage setSignupPasswordInput(String passwordInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting signup password input");
        WaitActions.typeIntoElement(signupPasswordInput, passwordInput);

        return this;
    }

    public SignupPage setSignupFirstnameInput(String firstnameInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting signup firstname input");
        WaitActions.typeIntoElement(signupFirstnameInput, firstnameInput);

        return this;
    }

    public SignupPage setSignupLastnameInput(String lastnameInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting signup lastname input");
        WaitActions.typeIntoElement(signupLastnameInput, lastnameInput);

        return this;
    }

    public SignupPage setSignupCompanyInput(String companyInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting signup company input");
        WaitActions.typeIntoElement(signupCompanyInput, companyInput);

        return this;
    }

    public SignupPage setSignupFirstAddressInput(String firstAddressInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting signup first address input");
        WaitActions.typeIntoElement(signupFirstAddressInput, firstAddressInput);

        return this;
    }

    public SignupPage setSignupSecondAddressInput(String secondAddressInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting signup second address input");
        WaitActions.typeIntoElement(signupSecondAddressInput, secondAddressInput);

        return this;
    }

    public SignupPage setSignupStateInput(String stateInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting signup state input");
        WaitActions.typeIntoElement(signupStateInput, stateInput);

        return this;
    }

    public SignupPage setSignupCityInput(String cityInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting signup city input");
        WaitActions.typeIntoElement(signupCityInput, cityInput);

        return this;
    }

    public SignupPage setSignupZipcodeInput(String zipcodeInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting signup zipcode input");
        WaitActions.typeIntoElement(signupZipcodeInput, zipcodeInput);

        return this;
    }

    public SignupPage setSignupMobileNumberInput(String mobileNumberInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting signup mobile number input");
        WaitActions.typeIntoElement(signupMobileNumberInput, mobileNumberInput);

        return this;
    }

    public SignupPage clickSignupButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the signup button");
        WaitActions.clickElement(signupButton);

        return this;
    }

    public SignupPage clickSignupNewsletterCheckbox() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on newsletter checkbox");
        WaitActions.clickElement(signupNewsletterCheckbox);

        return this;
    }

    public SignupPage clickSignupSpecialOffersCheckbox() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on special offers checkbox");
        WaitActions.clickElement(signupSpecialOffersCheckbox);

        return this;
    }

    public SignupPage clickSignupCreateAccountButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the create account button");
        WaitActions.clickElement(signupCreateAccountButton);

        return this;
    }

    public void clickSignupContinueButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the continue button");
        WaitActions.clickElement(signupContinueButton);
    }

    public SignupPage chooseSignupTitleCheckbox() {
        ExtentTestManager.getTest().log(Status.INFO, "Choosing of title");
        WaitActions.clickElement(signupTitleCheckbox);

        return this;
    }

    public SignupPage chooseSignupRandomDayInSelect() {
        ExtentTestManager.getTest().log(Status.INFO, "Choosing random day in date of birth");
        chooseRandomValueInSelect(signupDaysSelect);

        return this;
    }

    public SignupPage chooseSignupRandomMonthInSelect() {
        ExtentTestManager.getTest().log(Status.INFO, "Choosing random month in date of birth");
        chooseRandomValueInSelect(signupMonthsSelect);

        return this;
    }

    public SignupPage chooseSignupRandomYearInSelect() {
        ExtentTestManager.getTest().log(Status.INFO, "Choosing random year in date of birth");
        chooseRandomValueInSelect(signupYearsSelect);

        return this;
    }

    public SignupPage chooseSignupRandomCountryInSelect() {
        ExtentTestManager.getTest().log(Status.INFO, "Choosing random country");
        chooseRandomValueInSelect(signupCountrySelect);

        return this;
    }

    public SignupPage verifyHeaderBeforeRegistrationIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify 'New User Signup!' is visible");
        assertThat(signupHeaderTextBeforeRegistartion.getText()).isEqualTo("New User Signup!");

        return this;
    }

    public SignupPage verifyHeaderDuringRegistrationIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that 'ENTER ACCOUNT INFORMATION' is visible");
        assertThat(signupHeaderTextDuringRegistartion.getText()).isEqualTo("ENTER ACCOUNT INFORMATION");

        return this;
    }

    public SignupPage verifyHeaderAfterRegistrationIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that 'ACCOUNT CREATED!' is visible");
        assertThat(signupHeaderTextAfterRegistartion.getText()).isEqualTo("ACCOUNT CREATED!");

        return this;
    }

    public void verifyLoggedInTextUsernameIsVisibleSuccessfully(String username) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that 'Logged in as username' is visible");
        assertThat(signupLoggedInTextUsernameLink.getText()).isEqualTo("Logged in as " + username);
    }

    public void verifyErrorForEmailAlreadyExistIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify error 'Email Address already exist!' is visible");
        assertThat(signupErrorInformationEmailAlreadyText.getText()).isEqualTo("Email Address already exist!");
    }
}
