package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.TestSetup;
import utils.WaitActions;
import utils.extentreports.ExtentTestManager;
import utils.logs.Log;

import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class HomePage extends BasePage {

    private static final int FIRST_SUB_CATEGORY_INDEX = 0;
    private static final int MINIMUM_EXPECTED_CATEGORIES = 1;
    private static final Random RANDOM = new Random();
    @FindBy(css = "div.panel")
    protected List<WebElement> categoriesInLeftSideBar;
    @FindBy(css = "a[href='#Women']")
    protected WebElement womenCategory;
    @FindBy(css = "a[href='#Men']")
    protected WebElement menCategory;
    @FindBy(css = "#Women .panel-body li a")
    protected List<WebElement> womenSubCategoriesInLeftSideBar;
    @FindBy(css = "#Men .panel-body li a")
    protected List<WebElement> menSubCategoriesInLeftSideBar;
    @FindBy(css = "div.recommended_items h2.title")
    protected WebElement recommendedItemsText;
    @FindBy(css = "#recommended-item-carousel a.add-to-cart")
    protected List<WebElement> addToCartRecommendedItemsCarouselButton;
    @FindBy(css = "#slider-carousel h2")
    protected List<WebElement> headerInCarousel;
    @FindBy(css = "#scrollUp")
    protected WebElement scrollUp;
    @FindBy(css = "a[href='/login']")
    private WebElement signupLoginLink;
    @FindBy(css = "#checkoutModal a[href='/login']")
    private WebElement signupLoginLinkInCheckout;
    @FindBy(css = "a[href='/delete_account']")
    private WebElement deleteAccountLink;
    @FindBy(css = "a[href='/logout']")
    private WebElement logoutLink;
    @FindBy(css = "a[href='/']")
    private WebElement homeLink;
    @FindBy(css = "h2[data-qa='account-deleted']")
    private WebElement headerTextAccountDeleted;
    @FindBy(css = "a[href='/contact_us']")
    private WebElement contactUsLink;
    @FindBy(css = ".test_cases_list button")
    private WebElement testCasesButton;
    @FindBy(css = "a[href='/products']")
    private WebElement productsLink;
    @FindBy(css = "#footer h2")
    private WebElement headerSbuscriptionInFooter;
    @FindBy(id = "susbscribe_email")
    private WebElement subscriptionEmailInput;
    @FindBy(id = "subscribe")
    private WebElement subscriptionEmailButton;
    @FindBy(css = "div:not(.hide) > div.alert-success")
    private WebElement alertSuccessAfterSendSubscriptionEmail;
    @FindBy(css = "a[href='/view_cart']")
    private WebElement cartLink;
    @FindBy(css = "#cartModal a[href='/view_cart']")
    private WebElement recommendedItemsCartLink;

    public HomePage() {
        super();
    }

    public void clickSignupLogin() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the signup button");
        WaitActions.clickElement(signupLoginLink);
    }

    public void clickSignupLoginInCheckout() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the signup link in checkout");
        WaitActions.clickElement(signupLoginLinkInCheckout);
    }

    public HomePage clickDeleteAccountLink() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the delete account link");
        WaitActions.clickElement(deleteAccountLink);
        return this;
    }

    public HomePage clickLogoutLink() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the logout link");
        WaitActions.clickElement(logoutLink);
        return this;
    }

    public void clickHomeLink() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the home link");
        WaitActions.clickElement(homeLink);
    }

    public void verifyHeaderTextAccountDeletedIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that 'ACCOUNT DELETED!' is visible");
        assertThat(headerTextAccountDeleted.getText()).isEqualTo("ACCOUNT DELETED!");
    }

    public void clickContactUsLink() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the contact us link");
        WaitActions.clickElement(contactUsLink);
    }

    public void clickTestCasesButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the test cases button");
        WaitActions.clickElement(testCasesButton);
    }

    public void clickProductsLink() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the products link");
        WaitActions.clickElement(productsLink);
    }

    public HomePage verifyHeaderTextSubscriptionIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify text 'SUBSCRIPTION'");
        assertThat(headerSbuscriptionInFooter.getText()).isEqualTo("SUBSCRIPTION");
        return this;
    }

    public HomePage setSubscriptionEmailInput(String subscriptionEmail) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting subscription email input");
        WaitActions.typeIntoElement(subscriptionEmailInput, subscriptionEmail);

        return this;
    }

    public HomePage clickSubscriptionEmailButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the subscription email button");
        WaitActions.clickElement(subscriptionEmailButton);
        return this;
    }

    public void verifySuccessMessageAfterSendSubscriptionEmailIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(
                Status.INFO,
                "Verify success message 'You have been successfully subscribed!' is visible"
        );
        assertThat(alertSuccessAfterSendSubscriptionEmail.getText())
                .isEqualTo("You have been successfully subscribed!");
    }

    public HomePage clickCartLink() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the cart link");
        WaitActions.clickElement(cartLink);
        return this;
    }

    public int getAmountCategoriesInLeftSideBar() {
        ExtentTestManager.getTest().log(Status.INFO, "Get amount categories in left side bar");
        return categoriesInLeftSideBar.size();
    }

    public HomePage verifyThatCategoriesOnLeftSideBarAreVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that categories are visible on left side bar");
        assertThat(getAmountCategoriesInLeftSideBar() >= MINIMUM_EXPECTED_CATEGORIES).isTrue();
        return this;
    }

    public HomePage expandWomenCategory() {
        ExtentTestManager.getTest().log(Status.INFO, "Expand women category");
        WaitActions.clickElement(womenCategory);
        return this;
    }

    public HomePage expandMenCategory() {
        ExtentTestManager.getTest().log(Status.INFO, "Expand men category");
        WaitActions.clickElement(menCategory);
        return this;
    }

    public void clickFirstWomenSubCategoryInLeftSideBar() {
        ExtentTestManager.getTest().log(Status.INFO, "Click first women sub category in left side bar");
        WaitActions.clickElement(womenSubCategoriesInLeftSideBar.get(FIRST_SUB_CATEGORY_INDEX));
    }

    public void clickFirstMenSubCategoryInLeftSideBar() {
        ExtentTestManager.getTest().log(Status.INFO, "Click first men sub category in left side bar");
        WaitActions.clickElement(menSubCategoriesInLeftSideBar.get(FIRST_SUB_CATEGORY_INDEX));
    }

    public HomePage clickRandomVisibleItemFromRecommendedCarousel() {
        ExtentTestManager.getTest().log(Status.INFO, "Click random visible item from recommended carousel");
        List<WebElement> visibleElements = addToCartRecommendedItemsCarouselButton.stream()
                .filter(WebElement::isDisplayed)
                .toList();

        if (!visibleElements.isEmpty()) {
            int randomIndex = RANDOM.nextInt(visibleElements.size());

            WebElement elementToClick = visibleElements.get(randomIndex);
            elementToClick.click();
        } else {
            Log.info("No visible elements to click on");
        }
        return this;
    }

    public void verifyRandomVisibleHeaderInCarousel() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify random visible header text from carousel");

        List<WebElement> visibleElements = headerInCarousel.stream()
                .filter(WebElement::isDisplayed)
                .toList();

        if (!visibleElements.isEmpty()) {
            int randomIndex = RANDOM.nextInt(visibleElements.size());

            WebElement elementToVerify = visibleElements.get(randomIndex);
            String actualHeaderText = elementToVerify.getText();

            assertThat(actualHeaderText).isEqualTo("Full-Fledged practice website for Automation Engineers");
        } else {
            Log.info("No visible elements to verify");
        }
    }

    public void clickRecommendedItemsCartLink() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the recommended items cart link");
        WaitActions.clickElement(recommendedItemsCartLink);
    }

    public HomePage clickScrollUpButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the scroll up button");
        ((JavascriptExecutor) TestSetup.getDriverThreadLocal()).executeScript("arguments[0].click();", scrollUp);
        return this;
    }
}
