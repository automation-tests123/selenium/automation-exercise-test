package pages;

import com.aventstack.extentreports.Status;
import utils.TestSetup;
import utils.extentreports.ExtentTestManager;

import static org.assertj.core.api.Assertions.assertThat;

public class TestCasesPage extends BasePage {

    public TestCasesPage() {
        super();
    }

    public void verifyThatTestCasePageIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that test case page is visible successfully");
        assertThat(TestSetup.getDriverThreadLocal().getCurrentUrl())
                .isEqualTo("https://automationexercise.com/test_cases");
    }
}