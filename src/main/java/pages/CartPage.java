package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.SkipException;
import utils.TestSetup;
import utils.WaitActions;
import utils.extentreports.ExtentTestManager;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CartPage extends BasePage {

    private static final int FIRST_ITEM_INDEX = 0;
    @FindBy(css = "#cart_info_table tbody tr")
    private List<WebElement> listOfProductsInCart;
    @FindBy(css = "a.check_out")
    private WebElement proceedToCheckoutButton;
    @FindBy(css = "#address_delivery li.address_firstname.address_lastname")
    private WebElement deliveryFirstNameAndLastName;
    @FindBy(css = "#address_delivery li.address_city.address_state_name.address_postcode")
    private WebElement deliveryCityAndStateAndPostcode;
    @FindBy(css = "#address_delivery li.address_address1.address_address2:nth-of-type(4)")
    private WebElement deliveryFirstLineOfAddress;
    @FindBy(css = "#address_delivery li.address_address1.address_address2:nth-of-type(5)")
    private WebElement deliverySecondLineOfAddress;
    @FindBy(css = "#address_delivery li.address_phone")
    private WebElement deliveryPhoneNumber;
    @FindBy(css = "#address_invoice li.address_firstname.address_lastname")
    private WebElement billingFirstNameAndLastName;
    @FindBy(css = "#address_invoice li.address_city.address_state_name.address_postcode")
    private WebElement billingyCityAndStateAndPostcode;
    @FindBy(css = "#address_invoice li.address_address1.address_address2:nth-of-type(4)")
    private WebElement billingFirstLineOfAddress;
    @FindBy(css = "#address_invoice li.address_address1.address_address2:nth-of-type(5")
    private WebElement billingSecondLineOfAddress;
    @FindBy(css = "#address_invoice li.address_phone")
    private WebElement billingPhoneNumber;
    @FindBy(css = "#ordermsg textarea")
    private WebElement descriptionOfCheckoutTextarea;
    @FindBy(css = "input[data-qa='name-on-card']")
    private WebElement creditCardNameOnCardInput;
    @FindBy(css = "a.check_out")
    private WebElement placeOrderButton;
    @FindBy(css = "input[data-qa='card-number']")
    private WebElement creditCardNumberInput;
    @FindBy(css = "input[data-qa='cvc']")
    private WebElement creditCardNumberCVCInput;
    @FindBy(css = "input[data-qa='expiry-month']")
    private WebElement creditCardNumberExpiryMonthInput;
    @FindBy(css = "input[data-qa='expiry-year']")
    private WebElement creditCardNumberExpiryYearInput;
    @FindBy(css = "button[data-qa='pay-button']")
    private WebElement payAndConfirmOrderButton;
    @FindBy(css = "#form p")
    private WebElement orderConfirmedText;
    @FindBy(css = "a.cart_quantity_delete")
    private List<WebElement> delteProductFromCart;
    @FindBy(css = "#empty_cart p b")
    private WebElement cartIsEmptyText;
    @FindBy(css = "a.check_out")
    private WebElement downloadInvoiceButton;

    public CartPage() {
        super();
    }

    public int getAmountOfProductsInCart() {
        ExtentTestManager.getTest().log(Status.INFO, "Get amount of products in cart");
        return listOfProductsInCart.size();
    }

    public CartPage verifyNumberOfProductsAreVisibleSuccessfully(int numOfProducts) {
        if (getAmountOfProductsInCart() == numOfProducts) {
            ExtentTestManager.getTest().log(Status.INFO, "Verify products in cart are visible");
            assertThat(getAmountOfProductsInCart()).isEqualTo(numOfProducts);
        } else {
            throw new SkipException("There is no products available");
        }

        return this;
    }

    public String getPriceFromNumberOfProductPreview(int numOfProduct) {
        ProductsPage productsPage = new ProductsPage();
        return productsPage.priceOfProductInPreview.get(numOfProduct).getText();
    }

    public CartPage verifyThatPriceInCartPageIsSameAsInProductPreview(int productId, String priceFromProductPreview) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that price in cart page is same as in product preview");
        WebElement priceOfProductByProductId = TestSetup.getDriverThreadLocal()
                .findElement(By.cssSelector("tr#product-" + productId + " td.cart_price p"));

        assertThat(priceOfProductByProductId.getText()).isEqualTo(priceFromProductPreview);

        return this;
    }

    public CartPage verifyThatQuantityInCartPageIsCorrect(int productId, String quantity) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that quantity in cart page is correct");
        WebElement quantityOfProductByProductId = TestSetup.getDriverThreadLocal()
                .findElement(By.cssSelector("tr#product-" + productId + " td.cart_quantity button"));

        assertThat(quantityOfProductByProductId.getText()).isEqualTo(quantity);

        return this;
    }

    public CartPage verifyThatTotalPriceInCartPageIsCorrect(int productId, String priceFromProductPreview) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that total price in cart page is correct");
        WebElement quantityOfProductByProductId = TestSetup.getDriverThreadLocal()
                .findElement(By.cssSelector("tr#product-" + productId + " td.cart_total p"));

        assertThat(quantityOfProductByProductId.getText()).isEqualTo(priceFromProductPreview);

        return this;
    }

    public CartPage clickProceedToCheckoutButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the proceed to checkout button");
        WaitActions.clickElement(proceedToCheckoutButton);

        return this;
    }

    public CartPage verifyThatDeliveryFirstNameAndLastNameIsCorrect(String firstName, String lastName) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that delivery first name and last name are correct");

        assertThat(deliveryFirstNameAndLastName.getText()).containsIgnoringCase(firstName + " " + lastName);

        return this;
    }

    public CartPage verifyThatDeliveryCityAndStateAndPostcodeIsCorrect(String city, String state, String zipCode) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that delivery city, state and zipcode are correct");

        assertThat(deliveryCityAndStateAndPostcode.getText()).isEqualTo(city + " " + state + " " + zipCode);

        return this;
    }

    public CartPage verifyThatDeliveryFirstLineOfAddressIsCorrect(String firstAddress) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that delivery first address is correct");

        assertThat(deliveryFirstLineOfAddress.getText()).isEqualTo(firstAddress);

        return this;
    }

    public CartPage verifyThatDeliverySecondLineOfAddressIsCorrect(String secondAddress) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that delivery second address is correct");

        assertThat(deliverySecondLineOfAddress.getText()).isEqualTo(secondAddress);

        return this;
    }

    public CartPage verifyThatDeliveryPhoneNumberIsCorrect(String phoneNumber) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that delivery phone number is correct");

        assertThat(deliveryPhoneNumber.getText()).isEqualTo(phoneNumber);

        return this;
    }

    public CartPage verifyThatBillingFirstNameAndLastNameIsCorrect(String firstName, String lastName) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that billing first name and last name are correct");

        assertThat(billingFirstNameAndLastName.getText()).containsIgnoringCase(firstName + " " + lastName);

        return this;
    }

    public CartPage verifyThatBillingCityAndStateAndPostcodeIsCorrect(String city, String state, String zipCode) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that billing city, state and zipcode are correct");

        assertThat(billingyCityAndStateAndPostcode.getText()).isEqualTo(city + " " + state + " " + zipCode);

        return this;
    }

    public CartPage verifyThatBillingFirstLineOfAddressIsCorrect(String firstAddress) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that billing first address is correct");

        assertThat(billingFirstLineOfAddress.getText()).isEqualTo(firstAddress);

        return this;
    }

    public CartPage verifyThatBillingSecondLineOfAddressIsCorrect(String secondAddress) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that billing second address is correct");

        assertThat(billingSecondLineOfAddress.getText()).isEqualTo(secondAddress);

        return this;
    }

    public CartPage verifyThatBillingPhoneNumberIsCorrect(String phoneNumber) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that billing phone number is correct");

        assertThat(billingPhoneNumber.getText()).isEqualTo(phoneNumber);

        return this;
    }

    public CartPage setDescriptionCheckoutTextarea(String description) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting description checkout textarea");
        WaitActions.typeIntoElement(descriptionOfCheckoutTextarea, description);

        return this;
    }

    public CartPage clickPlaceOrderButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the place order button");
        WaitActions.clickElement(placeOrderButton);

        return this;
    }

    public CartPage setCreditCardNameOnCardInput(String nameOnCard) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting credit card name input");
        WaitActions.typeIntoElement(creditCardNameOnCardInput, nameOnCard);

        return this;
    }

    public CartPage setCreditCardNumberInput(String cardNumber) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting credit card number input");
        WaitActions.typeIntoElement(creditCardNumberInput, cardNumber);

        return this;
    }

    public CartPage setCreditCardNumberCVCInput(String cardNumberCVC) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting credit card number CVC input");
        WaitActions.typeIntoElement(creditCardNumberCVCInput, cardNumberCVC);

        return this;
    }

    public CartPage setCreditCardExpirationMonthInput(int expirationMonth) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting credit card expiration month input");
        WaitActions.typeIntoElement(creditCardNumberExpiryMonthInput, String.valueOf(expirationMonth));

        return this;
    }

    public CartPage setCreditCardExpirationYearInput(int expirationYear) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting credit card expiration year input");
        WaitActions.typeIntoElement(creditCardNumberExpiryYearInput, String.valueOf(expirationYear));

        return this;
    }

    public CartPage clickPayAndConfirmOrderButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the pay and confirm order button");
        WaitActions.clickElement(payAndConfirmOrderButton);

        return this;
    }

    public CartPage verifyMessageOfOrderHasBeenPlacedIsVisibleSuccessfully() {
        ExtentTestManager.getTest()
                .log(Status.INFO, "Verify success message 'Congratulations! Your order has been confirmed!'");
        assertThat(orderConfirmedText.getText()).isEqualTo("Congratulations! Your order has been confirmed!");

        return this;
    }

    public CartPage clickDeleteProductFromCartButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the delete product from cart button");
        WaitActions.clickElement(delteProductFromCart.get(FIRST_ITEM_INDEX));

        return this;
    }

    public CartPage clickDeleteAllProductsFromCartButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the all delete products from cart button");
        for (WebElement webElement : delteProductFromCart) {
            webElement.click();
        }

        return this;
    }

    public void verifyCartIsEmptyIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that product is removed from the cart");
        WaitActions waitActions = new WaitActions();
        waitActions.waitForElementAppear(cartIsEmptyText);
        assertThat(cartIsEmptyText.getText()).isEqualTo("Cart is empty!");
    }

    public void clickDownloadInvoiceButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the download invoice button");
        WaitActions.clickElement(downloadInvoiceButton);
    }
}