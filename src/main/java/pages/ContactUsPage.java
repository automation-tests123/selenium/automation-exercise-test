package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WaitActions;
import utils.extentreports.ExtentTestManager;

import static org.assertj.core.api.Assertions.assertThat;

public class ContactUsPage extends BasePage {

    @FindBy(css = ".contact-form h2")
    private WebElement contactUsFormHeader;

    @FindBy(css = "input[data-qa='name']")
    private WebElement contactUsNameInput;

    @FindBy(css = "input[data-qa='email']")
    private WebElement contactUsEmailInput;

    @FindBy(css = "input[data-qa='subject']")
    private WebElement contactUsSubjectInput;

    @FindBy(css = "textarea[data-qa='message']")
    private WebElement contactUsMessageTextarea;

    @FindBy(css = "input[name='upload_file']")
    private WebElement contactUsUploadFileInput;

    @FindBy(css = "input[data-qa='submit-button']")
    private WebElement contactUsSubmitButton;

    @FindBy(css = ".contact-form .alert-success")
    private WebElement contactUsSuccessText;

    @FindBy(css = ".contact-form .btn-success")
    private WebElement contactUsHomeButton;

    public ContactUsPage() {
        super();
    }

    public void verifyHeaderTextGetInTouchIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify 'GET IN TOUCH' is visible");
        assertThat(contactUsFormHeader.getText()).isEqualTo("GET IN TOUCH");
    }

    public ContactUsPage setContactUsNameInput(String nameInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting contact us name input");
        WaitActions.typeIntoElement(contactUsNameInput, nameInput);

        return this;
    }

    public ContactUsPage setContactUsEmailInput(String emailInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting contact us email input");
        WaitActions.typeIntoElement(contactUsEmailInput, emailInput);

        return this;
    }

    public ContactUsPage setContactUsSubjectInput(String subjectInput) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting contact us subject input");
        WaitActions.typeIntoElement(contactUsSubjectInput, subjectInput);

        return this;
    }

    public ContactUsPage setContactUsMessageTextarea(String messageTextarea) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting contact us message textarea");
        WaitActions.typeIntoElement(contactUsMessageTextarea, messageTextarea);

        return this;
    }

    public ContactUsPage setContactUsUploadFileInput(String filePath) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting contact us message textarea");
        WaitActions.typeIntoElement(contactUsUploadFileInput, filePath);

        return this;
    }

    public void clickSubmitButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the submit button");
        WaitActions.clickElement(contactUsSubmitButton);
    }

    public ContactUsPage verifyHeaderTextSuccessIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(
                Status.INFO,
                "Verify success message 'Success! Your details have been submitted successfully.' is visible"
        );
        assertThat(contactUsSuccessText.getText())
                .isEqualTo("Success! Your details have been submitted successfully.");
        return this;
    }

    public void clickHomeButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the home button");
        WaitActions.clickElement(contactUsHomeButton);
    }
}