package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.SkipException;
import utils.AppMainMethods;
import utils.TestSetup;
import utils.WaitActions;
import utils.extentreports.ExtentTestManager;

import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class ProductsPage extends BasePage {

    private static final int NO_PRODUCTS = 0;
    private static final int MINIMUM_EXPECTED_COUNT = 1;
    private static final int FIRST_ITEM_INDEX = 0;
    @FindBy(css = ".single-products .productinfo h2")
    protected List<WebElement> priceOfProductInPreview;
    @FindBy(id = "quantity")
    protected WebElement quantityInput;
    @FindBy(css = "button.cart")
    protected WebElement addToCartButton;
    @FindBy(css = "h2.title")
    protected WebElement headerOfFirstSubCategory;
    @FindBy(css = "div.brands-name ul li a")
    protected List<WebElement> brandsInLeftSideBar;
    @FindBy(css = "div.product-image-wrapper .nav a")
    protected List<WebElement> viewProductButton;
    @FindBy(css = "a[href='#reviews']")
    protected WebElement writeYourReviewText;
    @FindBy(css = "#review-form #name")
    protected WebElement nameFormInput;
    @FindBy(css = "#review-form #email")
    protected WebElement emailFormInput;
    @FindBy(css = "#review-form #review")
    protected WebElement reviewFormTextArea;
    @FindBy(css = "#review-form #button-review")
    protected WebElement submitFormButton;
    @FindBy(css = ".alert-success")
    protected WebElement successMessage;
    @FindBy(css = ".features_items h2.title")
    private WebElement productsHeader;
    @FindBy(css = "div.product-image-wrapper")
    private List<WebElement> listOfAllProducts;
    @FindBy(css = "a[href='/product_details/1']")
    private WebElement firstViewProduct;
    @FindBy(css = ".product-information h2")
    private WebElement nameOfProductInProductDetails;
    @FindBy(css = "div.product-information p:first-of-type")
    private WebElement categoryOfProductInProductDetails;
    @FindBy(css = "div.product-information span span")
    private WebElement priceOfProductInProductDetails;
    @FindBy(css = "div.product-information p:nth-of-type(2)")
    private WebElement availabilityOfProductInProductDetails;
    @FindBy(css = "div.product-information p:nth-of-type(3)")
    private WebElement conditionOfProductInProductDetails;
    @FindBy(css = "div.product-information p:nth-of-type(4)")
    private WebElement brandOfProductInProductDetails;
    @FindBy(id = "search_product")
    private WebElement searchProductEngineInput;
    @FindBy(id = "submit_search")
    private WebElement productSearchEngineButton;
    @FindBy(css = "div.productinfo p:first-of-type")
    private WebElement nameOfProductInPreview;
    @FindBy(css = "#cartModal .btn-success")
    private WebElement continueShoppingButton;
    @FindBy(css = "#cartModal a[href='/view_cart']")
    private WebElement viewCartLink;

    public ProductsPage() {
        super();
    }

    public ProductsPage verifyThatProductsPageIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify user is navigated to ALL PRODUCTS page successfully");
        assertThat(TestSetup.getDriverThreadLocal().getCurrentUrl()).isEqualTo("https://automationexercise.com/products");

        return this;
    }

    public ProductsPage verifyHeaderAllProductsIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify 'ALL PRODUCTS' header is visible");
        assertThat(productsHeader.getText()).isEqualTo("ALL PRODUCTS");

        return this;
    }

    public int getAmountOfAllProducts() {
        ExtentTestManager.getTest().log(Status.INFO, "Get amount of all products");
        return listOfAllProducts.size();
    }

    public void verifyAllProductsAreVisibleSuccessfully() {
        if (getAmountOfAllProducts() != NO_PRODUCTS) {
            ExtentTestManager.getTest().log(Status.INFO, "Verify 'ALL PRODUCTS' are visible");
            assertThat(getAmountOfAllProducts()).isGreaterThan(NO_PRODUCTS);
        } else {
            throw new SkipException("There is no products available");
        }
    }

    public ProductsPage clickOnFirstViewProduct() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the first view product");
        WaitActions.clickElement(firstViewProduct);

        return this;
    }

    public ProductsPage verifyThatProductDetailsPageIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify user is landed to product detail page successfully");
        assertThat(TestSetup.getDriverThreadLocal().getCurrentUrl())
                .isEqualTo("https://automationexercise.com/product_details/1");

        return this;
    }

    public ProductsPage verifyThatDetailsOfFirstProductAreVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that details are visible: product name, category, " +
                "price, availability, condition, brand");
        assertThat(nameOfProductInProductDetails.getText()).isEqualTo("Blue Top");
        assertThat(categoryOfProductInProductDetails.getText()).isEqualTo("Category: Women > Tops");
        assertThat(priceOfProductInProductDetails.getText()).isEqualTo("Rs. 500");
        assertThat(availabilityOfProductInProductDetails.getText()).isEqualTo("Availability: In Stock");
        assertThat(conditionOfProductInProductDetails.getText()).isEqualTo("Condition: New");
        assertThat(brandOfProductInProductDetails.getText()).isEqualTo("Brand: Polo");

        return this;
    }

    public ProductsPage setSearchProductEngineInput(String searchedProduct) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting search product engine input");
        WaitActions.typeIntoElement(searchProductEngineInput, searchedProduct);

        return this;
    }

    public ProductsPage clickSearchProductButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking search product button");
        WaitActions.clickElement(productSearchEngineButton);

        return this;
    }

    public ProductsPage verifyHeaderSearchedProductsIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify 'ALL PRODUCTS' header is visible");
        assertThat(productsHeader.getText()).isEqualTo("SEARCHED PRODUCTS");

        return this;
    }

    public void verifyFirstWomenSubCategoryHeaderIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(
                Status.INFO,
                "Verify that category page is displayed and confirm text 'WOMEN - DRESS PRODUCTS'"
        );
        assertThat(headerOfFirstSubCategory.getText()).isEqualTo("WOMEN - DRESS PRODUCTS");
    }

    public void verifyFirstMenSubCategoryHeaderIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(
                Status.INFO,
                "Verify that category page is displayed and confirm text 'MEN - TSHIRTS PRODUCTS'"
        );
        assertThat(headerOfFirstSubCategory.getText()).isEqualTo("MEN - TSHIRTS PRODUCTS");
    }

    public ProductsPage verifyAllSearchedProductsAreVisibleSuccessfully(int numOfSearchedProducts) {
        if (getAmountOfAllProducts() != NO_PRODUCTS) {
            ExtentTestManager.getTest().log(Status.INFO, "Verify all the products related to search are visible");
            assertThat(getAmountOfAllProducts()).isEqualTo(numOfSearchedProducts);
        } else {
            throw new SkipException("There is no searched products available");
        }

        return this;
    }

    public ProductsPage verifyNameOfProductInPreviewIsSameAsSearchedProdcutSuccessfully(String searchedProduct) {
        ExtentTestManager.getTest().log(Status.INFO, "Verify name of product is visible");
        assertThat(nameOfProductInPreview.getText()).isEqualTo(searchedProduct);

        return this;
    }

    public ProductsPage hoverProduct(int numOfProduct) {
        AppMainMethods appMainMethods = new AppMainMethods();

        appMainMethods.hoverOverElement(listOfAllProducts.get(numOfProduct));

        return this;
    }

    public ProductsPage clickAddToCartByProductId(int productId) {
        WebElement addToCartByProductId = TestSetup.getDriverThreadLocal()
                .findElement(By.cssSelector("a[data-product-id='" + productId + "']"));
        addToCartByProductId.click();

        return this;
    }

    public ProductsPage clickContinueShoppingButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the continue shopping button");
        WaitActions.clickElement(continueShoppingButton);

        return this;
    }

    public void clickViewCart() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking on the view cart");
        WaitActions.clickElement(viewCartLink);
    }

    public ProductsPage setQuantity(String numOfQuantity) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting quantity");
        quantityInput.clear();
        WaitActions.typeIntoElement(quantityInput, numOfQuantity);

        return this;
    }

    public ProductsPage clickAddToCart() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking add to cart");
        WaitActions.clickElement(addToCartButton);

        return this;
    }

    public int getAmountBrandsInLeftSideBar() {
        ExtentTestManager.getTest().log(Status.INFO, "Get amount brands in left side bar");
        return brandsInLeftSideBar.size();
    }

    public ProductsPage verifyThatBrandsOnLeftSideBarAreVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that brands are visible on left side bar");
        assertThat(getAmountBrandsInLeftSideBar() >= MINIMUM_EXPECTED_COUNT).isTrue();
        return this;
    }

    public ProductsPage verifyThatProducstInBrandAreVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify that there are products in brand");
        assertThat(getAmountOfAllProducts() >= MINIMUM_EXPECTED_COUNT).isTrue();
        return this;
    }

    public ProductsPage clickRandomBrand() {
        ExtentTestManager.getTest().log(Status.INFO, "Click on any brand name");
        if (!brandsInLeftSideBar.isEmpty()) {
            Random rand = new Random();
            int randomIndex = rand.nextInt(brandsInLeftSideBar.size());

            brandsInLeftSideBar.get(randomIndex).click();
        } else {
            throw new SkipException("List of brands in left bar is empty");
        }

        return this;
    }

    public ProductsPage verifyHeaderContainsBrandIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify header contains 'BRAND'");
        assertThat(productsHeader.getText()).containsIgnoringCase("BRAND");

        return this;
    }

    public ProductsPage clickFirstViewProduct() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking first view product");
        AppMainMethods appMainMethods = new AppMainMethods();
        appMainMethods.scrollToElement(viewProductButton.get(FIRST_ITEM_INDEX));
        WaitActions.clickElement(viewProductButton.get(FIRST_ITEM_INDEX));

        return this;
    }

    public ProductsPage verifyWriteYourReviewIsVisibleSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify 'Write Your Review' is visible");
        assertThat(writeYourReviewText.getText()).isEqualTo("WRITE YOUR REVIEW");

        return this;
    }

    public ProductsPage setNameFormInput(String name) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting name form input");
        WaitActions.typeIntoElement(nameFormInput, name);

        return this;
    }

    public ProductsPage setEmailFormInput(String email) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting email form input");
        WaitActions.typeIntoElement(emailFormInput, email);

        return this;
    }

    public ProductsPage setReviewFormTextarea(String review) {
        ExtentTestManager.getTest().log(Status.INFO, "Setting review form textarea");
        WaitActions.typeIntoElement(reviewFormTextArea, review);

        return this;
    }

    public ProductsPage clickSubmitFormButton() {
        ExtentTestManager.getTest().log(Status.INFO, "Clicking submit form button");
        AppMainMethods appMainMethods = new AppMainMethods();
        appMainMethods.scrollToElement(submitFormButton);
        WaitActions.clickElement(submitFormButton);

        return this;
    }

    public void verifySuccessMessageForReviewSuccessfully() {
        ExtentTestManager.getTest().log(Status.INFO, "Verify success message 'Thank you for your review.'");
        assertThat(successMessage.getText()).isEqualTo("Thank you for your review.");
    }
}