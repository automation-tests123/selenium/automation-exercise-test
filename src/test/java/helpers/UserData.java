package helpers;

import lombok.Getter;

@Getter
public class UserData {
    private final String email;
    private final String password;
    private final String name;
    private final String firstName;
    private final String lastName;
    private final String firstAddressInput;
    private final String secondAddressInput;
    private final String stateInput;
    private final String cityInput;
    private final String zipcodeInput;
    private final String mobileNumberInput;

    private UserData(Builder builder) {
        this.email = builder.email;
        this.password = builder.password;
        this.name = builder.name;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.firstAddressInput = builder.firstAddressInput;
        this.secondAddressInput = builder.secondAddressInput;
        this.stateInput = builder.stateInput;
        this.cityInput = builder.cityInput;
        this.zipcodeInput = builder.zipcodeInput;
        this.mobileNumberInput = builder.mobileNumberInput;
    }

    public static class Builder {
        private String email;
        private String password;
        private String name;
        private String firstName;
        private String lastName;
        private String firstAddressInput;
        private String secondAddressInput;
        private String stateInput;
        private String cityInput;
        private String zipcodeInput;
        private String mobileNumberInput;

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder setFirstAddressInput(String firstAddressInput) {
            this.firstAddressInput = firstAddressInput;
            return this;
        }

        public Builder setSecondAddressInput(String secondAddressInput) {
            this.secondAddressInput = secondAddressInput;
            return this;
        }

        public Builder setStateInput(String stateInput) {
            this.stateInput = stateInput;
            return this;
        }

        public Builder setCityInput(String cityInput) {
            this.cityInput = cityInput;
            return this;
        }

        public Builder setZipcodeInput(String zipcodeInput) {
            this.zipcodeInput = zipcodeInput;
            return this;
        }

        public Builder setMobileNumberInput(String mobileNumberInput) {
            this.mobileNumberInput = mobileNumberInput;
            return this;
        }

        public UserData build() {
            return new UserData(this);
        }
    }
}
