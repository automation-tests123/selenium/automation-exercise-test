package tests;

import com.github.javafaker.Faker;
import helpers.UserData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.*;
import utils.AppMainMethods;
import utils.TestSetup;
import utils.WaitActions;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;


public class CartTest extends TestSetup {

    private static final int FIRST_PRODUCT_ID = 1;
    private static final int SECOND_PRODUCT_ID = 2;
    private static final int FIRST_PRODUCT_INDEX = 0;
    private static final int SECOND_PRODUCT_INDEX = 1;
    private static final String QUANTITY_ONE = "1";
    private static final String QUANTITY_FOUR = "4";
    private static final int DESCRIPTION_LENGTH = 256;
    private static final int CREDIT_CARD_CVC_NUMBER_LENGTH = 3;
    private static final int MIN_CREDIT_CARD_EXPIRATION_MONTH = 1;
    private static final int MAX_CREDIT_CARD_EXPIRATION_MONTH = 12;
    private static final int MIN_CREDIT_CARD_EXPIRATION_YEAR = 2025;
    private static final int MAX_CREDIT_CARD_EXPIRATION_YEAR = 2030;
    private static final int SINGLE_PRODUCT = 1;
    private static final int TWO_PRODUCTS = 2;
    private static final int FILE_WAIT_TIME = 15;
    private AppMainMethods appMainMethods;
    private HomePage homePage;
    private Faker faker;
    private ProductsPage productsPage;
    private CartPage cartPage;
    private RegisterUserTest registerUserTest;
    private SignupPage signupPage;
    private LoginPage loginPage;
    private String email;
    private String password;
    private String name;
    private String firstName;
    private String lastName;
    private String firstAddressInput;
    private String secondAddressInput;
    private String stateInput;
    private String cityInput;
    private String zipcodeInput;
    private String mobileNumberInput;
    private String description;
    private String creditCardNumber;
    private String creditCardCVCNumber;
    private int creditCardExpirationMonth;
    private int creditCardExpirationYear;

    private void registerUserAndSetData() {
        UserData userData = registerUserTest.registerUserWithoutDeleteAccountAndGetUserData();

        email = userData.getEmail();
        password = userData.getPassword();
        name = userData.getName();
        firstName = userData.getFirstName();
        lastName = userData.getLastName();
        firstAddressInput = userData.getFirstAddressInput();
        secondAddressInput = userData.getSecondAddressInput();
        stateInput = userData.getStateInput();
        cityInput = userData.getCityInput();
        zipcodeInput = userData.getZipcodeInput();
        mobileNumberInput = userData.getMobileNumberInput();
        description = faker.lorem().fixedString(DESCRIPTION_LENGTH);
        creditCardNumber = faker.finance().creditCard();
        creditCardCVCNumber = faker.number().digits(CREDIT_CARD_CVC_NUMBER_LENGTH);
        creditCardExpirationMonth = faker.number()
                .numberBetween(MIN_CREDIT_CARD_EXPIRATION_MONTH, MAX_CREDIT_CARD_EXPIRATION_MONTH);
        creditCardExpirationYear = faker.number()
                .numberBetween(MIN_CREDIT_CARD_EXPIRATION_YEAR, MAX_CREDIT_CARD_EXPIRATION_YEAR);
    }

    private void finalizeOrder() {
        cartPage.clickProceedToCheckoutButton()
                .verifyThatDeliveryFirstNameAndLastNameIsCorrect(firstName, lastName)
                .verifyThatDeliveryCityAndStateAndPostcodeIsCorrect(cityInput, stateInput, zipcodeInput)
                .verifyThatDeliveryFirstLineOfAddressIsCorrect(firstAddressInput)
                .verifyThatDeliverySecondLineOfAddressIsCorrect(secondAddressInput)
                .verifyThatDeliveryPhoneNumberIsCorrect(mobileNumberInput)
                .verifyThatBillingFirstNameAndLastNameIsCorrect(firstName, lastName)
                .verifyThatBillingCityAndStateAndPostcodeIsCorrect(cityInput, stateInput, zipcodeInput)
                .verifyThatBillingFirstLineOfAddressIsCorrect(firstAddressInput)
                .verifyThatBillingSecondLineOfAddressIsCorrect(secondAddressInput)
                .verifyThatBillingPhoneNumberIsCorrect(mobileNumberInput)
                .setDescriptionCheckoutTextarea(description)
                .clickPlaceOrderButton()
                .setCreditCardNameOnCardInput(firstName + " " + lastName)
                .setCreditCardNumberInput(creditCardNumber)
                .setCreditCardNumberCVCInput(creditCardCVCNumber)
                .setCreditCardExpirationMonthInput(creditCardExpirationMonth)
                .setCreditCardExpirationYearInput(creditCardExpirationYear)
                .clickPayAndConfirmOrderButton()
                .verifyMessageOfOrderHasBeenPlacedIsVisibleSuccessfully();

        homePage.clickDeleteAccountLink()
                .verifyHeaderTextAccountDeletedIsVisibleSuccessfully();

        signupPage.clickSignupContinueButton();
    }

    private void addProductToCartAndVerify() {
        productsPage.hoverProduct(FIRST_PRODUCT_ID)
                .clickAddToCartByProductId(FIRST_PRODUCT_ID)
                .clickContinueShoppingButton();

        homePage.clickCartLink();

        cartPage.verifyNumberOfProductsAreVisibleSuccessfully(SINGLE_PRODUCT)
                .verifyThatQuantityInCartPageIsCorrect(FIRST_PRODUCT_ID, QUANTITY_ONE);
    }

    @BeforeMethod
    public void setupBeforeMethod() {
        appMainMethods = new AppMainMethods();
        homePage = new HomePage();
        faker = new Faker();
        productsPage = new ProductsPage();
        cartPage = new CartPage();
        registerUserTest = new RegisterUserTest();
        registerUserTest.initializeDependenciesAndVariables();
        signupPage = new SignupPage();
        loginPage = new LoginPage();
    }

    @Test(description = "Verify Subscription in Cart page")
    public void verifySubscriptionInCartPageSuccessfully() {
        String subscriptionEmailInput = faker.internet().emailAddress();

        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickCartLink()
                .verifyHeaderTextSubscriptionIsVisibleSuccessfully()
                .setSubscriptionEmailInput(subscriptionEmailInput)
                .clickSubscriptionEmailButton()
                .verifySuccessMessageAfterSendSubscriptionEmailIsVisibleSuccessfully();
    }

    @Test(description = "Add Products in Cart")
    public void addProductsInCartCorrectly() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickProductsLink();

        String priceOfFirstProduct = cartPage.getPriceFromNumberOfProductPreview(FIRST_PRODUCT_INDEX);
        String priceOfSecondProduct = cartPage.getPriceFromNumberOfProductPreview(SECOND_PRODUCT_INDEX);

        productsPage.hoverProduct(FIRST_PRODUCT_ID)
                .clickAddToCartByProductId(FIRST_PRODUCT_ID)
                .clickContinueShoppingButton()
                .hoverProduct(SECOND_PRODUCT_ID)
                .clickAddToCartByProductId(SECOND_PRODUCT_ID)
                .clickViewCart();

        cartPage.verifyNumberOfProductsAreVisibleSuccessfully(TWO_PRODUCTS)
                .verifyThatPriceInCartPageIsSameAsInProductPreview(FIRST_PRODUCT_ID, priceOfFirstProduct)
                .verifyThatPriceInCartPageIsSameAsInProductPreview(SECOND_PRODUCT_ID, priceOfSecondProduct)
                .verifyThatQuantityInCartPageIsCorrect(FIRST_PRODUCT_ID, QUANTITY_ONE)
                .verifyThatQuantityInCartPageIsCorrect(SECOND_PRODUCT_ID, QUANTITY_ONE)
                .verifyThatTotalPriceInCartPageIsCorrect(FIRST_PRODUCT_ID, priceOfFirstProduct)
                .verifyThatTotalPriceInCartPageIsCorrect(SECOND_PRODUCT_ID, priceOfSecondProduct)
                .clickDeleteAllProductsFromCartButton()
                .verifyCartIsEmptyIsVisibleSuccessfully();
    }

    @Test(description = "Add Products in Cart")
    public void verifyProductQuantity() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        productsPage.clickOnFirstViewProduct()
                .verifyThatProductDetailsPageIsVisibleSuccessfully()
                .verifyThatDetailsOfFirstProductAreVisibleSuccessfully()
                .setQuantity(QUANTITY_FOUR)
                .clickAddToCart()
                .clickViewCart();

        cartPage.verifyNumberOfProductsAreVisibleSuccessfully(SINGLE_PRODUCT)
                .verifyThatQuantityInCartPageIsCorrect(FIRST_PRODUCT_ID, QUANTITY_FOUR)
                .clickDeleteProductFromCartButton()
                .verifyCartIsEmptyIsVisibleSuccessfully();
    }

    @Test(description = "Place Order: Register while Checkout")
    public void placeOrderRegisterWhileCheckout() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        addProductToCartAndVerify();

        cartPage.clickProceedToCheckoutButton();

        homePage.clickSignupLoginInCheckout();

        registerUserAndSetData();

        homePage.clickCartLink();

        finalizeOrder();
    }

    @Test(description = "Place Order: Register while Checkout")
    public void placeOrderRegisterBeforeCheckout() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickSignupLogin();

        registerUserAndSetData();

        addProductToCartAndVerify();

        finalizeOrder();
    }

    @Test(description = "Place Order: Login before Checkout")
    public void placeOrderLoginBeforeCheckout() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickSignupLogin();

        registerUserAndSetData();

        homePage.clickLogoutLink();

        appMainMethods.verifyThatLoginPageIsVisibleSuccessfully();

        homePage.clickSignupLogin();

        loginPage.verifyHeaderBeforeLoginIsVisibleSuccessfully()
                .setLoginEmailInput(email)
                .setLoginPasswordInput(password)
                .clickLoginButton()
                .verifyLoggedUsernameIsVisibleSuccessfully(name);

        addProductToCartAndVerify();

        finalizeOrder();
    }

    @Test(description = "Remove Products From Cart")
    public void removeProductsFromCart() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        addProductToCartAndVerify();

        cartPage.clickDeleteProductFromCartButton()
                .verifyCartIsEmptyIsVisibleSuccessfully();
    }

    @Test(description = "Search Products and Verify Cart After Login")
    @Parameters({"searchedProduct"})
    public void searchProductsAndVerifyCartAfterLogin(String searchedProduct) {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickCartLink();

        cartPage.clickDeleteAllProductsFromCartButton()
                .verifyCartIsEmptyIsVisibleSuccessfully();

        appMainMethods.clearCookiesAndRefresh();

        homePage.clickProductsLink();

        productsPage.verifyThatProductsPageIsVisibleSuccessfully()
                .verifyHeaderAllProductsIsVisibleSuccessfully()
                .setSearchProductEngineInput(searchedProduct)
                .clickSearchProductButton()
                .verifyHeaderSearchedProductsIsVisibleSuccessfully()
                .verifyNameOfProductInPreviewIsSameAsSearchedProdcutSuccessfully(searchedProduct)
                .hoverProduct(FIRST_PRODUCT_INDEX)
                .clickAddToCartByProductId(SECOND_PRODUCT_ID)
                .clickContinueShoppingButton();

        homePage.clickCartLink();

        cartPage.verifyNumberOfProductsAreVisibleSuccessfully(SINGLE_PRODUCT)
                .verifyThatQuantityInCartPageIsCorrect(SECOND_PRODUCT_ID, QUANTITY_ONE);

        homePage.clickSignupLogin();

        registerUserAndSetData();

        homePage.clickLogoutLink();

        appMainMethods.verifyThatLoginPageIsVisibleSuccessfully();

        homePage.clickSignupLogin();

        loginPage.verifyHeaderBeforeLoginIsVisibleSuccessfully()
                .setLoginEmailInput(email)
                .setLoginPasswordInput(password)
                .clickLoginButton()
                .verifyLoggedUsernameIsVisibleSuccessfully(name);

        homePage.clickCartLink();

        cartPage.verifyNumberOfProductsAreVisibleSuccessfully(SINGLE_PRODUCT)
                .verifyThatQuantityInCartPageIsCorrect(SECOND_PRODUCT_ID, QUANTITY_ONE);

        homePage.clickDeleteAccountLink()
                .verifyHeaderTextAccountDeletedIsVisibleSuccessfully();

        signupPage.clickSignupContinueButton();
    }

    @Test(description = "Add to cart from Recommended items")
    public void addToCartFromRecommendedItems() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully()
                .scrollToTheBottomOfThePage();

        homePage.clickRandomVisibleItemFromRecommendedCarousel()
                .clickRecommendedItemsCartLink();

        cartPage.verifyNumberOfProductsAreVisibleSuccessfully(SINGLE_PRODUCT);
    }

    @Test(description = "Download Invoice after purchase order")
    public void downloadInvoiceAfterPurchaseOrder() throws InterruptedException {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickCartLink();

        cartPage.clickDeleteAllProductsFromCartButton()
                .verifyCartIsEmptyIsVisibleSuccessfully();

        appMainMethods.clearCookiesAndRefresh();

        homePage.clickHomeLink();

        addProductToCartAndVerify();

        cartPage.clickProceedToCheckoutButton();

        homePage.clickSignupLoginInCheckout();

        registerUserAndSetData();

        homePage.clickCartLink();

        cartPage.clickProceedToCheckoutButton()
                .verifyThatDeliveryFirstNameAndLastNameIsCorrect(firstName, lastName)
                .verifyThatDeliveryCityAndStateAndPostcodeIsCorrect(cityInput, stateInput, zipcodeInput)
                .verifyThatDeliveryFirstLineOfAddressIsCorrect(firstAddressInput)
                .verifyThatDeliverySecondLineOfAddressIsCorrect(secondAddressInput)
                .verifyThatDeliveryPhoneNumberIsCorrect(mobileNumberInput)
                .verifyThatBillingFirstNameAndLastNameIsCorrect(firstName, lastName)
                .verifyThatBillingCityAndStateAndPostcodeIsCorrect(cityInput, stateInput, zipcodeInput)
                .verifyThatBillingFirstLineOfAddressIsCorrect(firstAddressInput)
                .verifyThatBillingSecondLineOfAddressIsCorrect(secondAddressInput)
                .verifyThatBillingPhoneNumberIsCorrect(mobileNumberInput)
                .setDescriptionCheckoutTextarea(description)
                .clickPlaceOrderButton()
                .setCreditCardNameOnCardInput(firstName + " " + lastName)
                .setCreditCardNumberInput(creditCardNumber)
                .setCreditCardNumberCVCInput(creditCardCVCNumber)
                .setCreditCardExpirationMonthInput(creditCardExpirationMonth)
                .setCreditCardExpirationYearInput(creditCardExpirationYear)
                .clickPayAndConfirmOrderButton()
                .verifyMessageOfOrderHasBeenPlacedIsVisibleSuccessfully()
                .clickDownloadInvoiceButton();

        File file = new File("src/test/resources/downloaded-file/invoice.txt");
        WaitActions waitActions = new WaitActions();

        String filePath = "src/test/resources/downloaded-file/invoice.txt";
        boolean isDownloaded = waitActions.waitForFile(filePath, FILE_WAIT_TIME);

        assertThat(isDownloaded).isTrue();

        homePage.clickDeleteAccountLink()
                .verifyHeaderTextAccountDeletedIsVisibleSuccessfully();

        signupPage.clickSignupContinueButton();

        if (file.exists()) {
            boolean isDeleted = file.delete();
            if (!isDeleted) {
                System.out.println("Failed to delete file: " + file.getPath());
            }
        }
    }
}
