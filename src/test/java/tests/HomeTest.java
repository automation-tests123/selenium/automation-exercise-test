package tests;

import com.github.javafaker.Faker;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import utils.AppMainMethods;
import utils.TestSetup;


public class HomeTest extends TestSetup {

    private AppMainMethods appMainMethods;
    private HomePage homePage;
    private Faker faker;

    @BeforeMethod
    public void setupBeforeMethod() {
        appMainMethods = new AppMainMethods();
        homePage = new HomePage();
        faker = new Faker();
    }

    @Test(description = "Verify test cases page")
    public void verifySubscriptionSuccessfully() {
        String subscriptionEmailInput = faker.internet().emailAddress();

        appMainMethods.verifyThatHomePageIsVisibleSuccessfully()
                .scrollToTheBottomOfThePage();

        homePage.verifyHeaderTextSubscriptionIsVisibleSuccessfully()
                .setSubscriptionEmailInput(subscriptionEmailInput)
                .clickSubscriptionEmailButton()
                .verifySuccessMessageAfterSendSubscriptionEmailIsVisibleSuccessfully();
    }

    @Test(description = "Verify Scroll Up using 'Arrow' button and Scroll Down functionality")
    public void verifyScrollUpUsingArrowButtonAndScrollDownFunctionality() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully()
                .scrollToTheBottomOfThePage();

        homePage.verifyHeaderTextSubscriptionIsVisibleSuccessfully()
                .clickScrollUpButton()
                .verifyRandomVisibleHeaderInCarousel();
    }

    @Test(description = "Verify Scroll Up without 'Arrow' button and Scroll Down functionality")
    public void verifyScrollUpWithoutArrowButtonAndScrollDownFunctionality() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully()
                .scrollToTheBottomOfThePage();

        homePage.verifyHeaderTextSubscriptionIsVisibleSuccessfully();

        appMainMethods.scrollToTheTopOfThePage();

        homePage.verifyRandomVisibleHeaderInCarousel();
    }
}
