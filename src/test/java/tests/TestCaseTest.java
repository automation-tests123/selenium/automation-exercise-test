package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.TestCasesPage;
import utils.AppMainMethods;
import utils.TestSetup;


public class TestCaseTest extends TestSetup {

    private AppMainMethods appMainMethods;
    private HomePage homePage;
    private TestCasesPage testCasePage;

    @BeforeMethod
    public void setupBeforeMethod() {
        appMainMethods = new AppMainMethods();
        homePage = new HomePage();
        testCasePage = new TestCasesPage();
    }

    @Test(description = "Verify test cases page")
    public void verifyTestCasesPageSuccessfully() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickTestCasesButton();

        testCasePage.verifyThatTestCasePageIsVisibleSuccessfully();
    }
}
