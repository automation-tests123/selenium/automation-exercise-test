package tests;

import com.github.javafaker.Faker;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.ContactUsPage;
import pages.HomePage;
import utils.AppMainMethods;
import utils.TestSetup;

import java.io.File;
import java.time.Duration;


public class ContactUsTest extends TestSetup {

    private static final int SUBJECT_WORD_COUNT = 5;
    private static final int MESSAGE_WORD_COUNT = 64;
    private Faker faker;
    private AppMainMethods appMainMethods;
    private HomePage homePage;
    private ContactUsPage contactUsPage;

    @BeforeMethod
    public void setupBeforeMethod() {
        faker = new Faker();
        appMainMethods = new AppMainMethods();
        homePage = new HomePage();
        contactUsPage = new ContactUsPage();
    }

    @Test(description = "Contact us form")
    @Parameters({"filePath"})
    public void sendContactUsFormCorrectly(String filePath) {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickContactUsLink();

        contactUsPage.verifyHeaderTextGetInTouchIsVisibleSuccessfully();

        String name = faker.name().name();
        String email = faker.internet().emailAddress();
        String subject = faker.lorem().sentence(SUBJECT_WORD_COUNT);
        String message = faker.lorem().sentence(MESSAGE_WORD_COUNT);

        File file = new File(filePath);

        contactUsPage.setContactUsNameInput(name)
                .setContactUsEmailInput(email)
                .setContactUsSubjectInput(subject)
                .setContactUsUploadFileInput(file.getAbsolutePath())
                .setContactUsMessageTextarea(message)
                .clickSubmitButton();

        try {
            FluentWait<ChromeDriver> wait = new FluentWait<>(TestSetup.getDriverThreadLocal())
                    .withTimeout(Duration.ofSeconds(10))
                    .pollingEvery(Duration.ofMillis(500))
                    .ignoring(NoAlertPresentException.class);

            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = TestSetup.getDriverThreadLocal().switchTo().alert();
            alert.accept();
        } catch (TimeoutException e) {
            System.err.println("No alert was present after the fluent wait.");
        }

        contactUsPage.verifyHeaderTextSuccessIsVisibleSuccessfully()
                .clickHomeButton();

        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();
    }
}