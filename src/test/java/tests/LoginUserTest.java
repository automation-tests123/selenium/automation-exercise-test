package tests;

import com.github.javafaker.Faker;
import helpers.UserData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import utils.AppMainMethods;
import utils.TestSetup;

import java.lang.reflect.Method;
import java.util.Arrays;


public class LoginUserTest extends TestSetup {

    private static final int MIN_PASSWORD_LENGTH = 8;
    private static final int MAX_PASSWORD_LENGTH = 12;
    private Faker faker;
    private AppMainMethods appMainMethods;
    private HomePage homePage;
    private LoginPage loginPage;
    private RegisterUserTest registerUserTest;
    private String emailInput;
    private String passwordInput;
    private UserData userData;

    private void performGroupSpecificSetup(Method method) {
        String[] groups = method.getAnnotation(Test.class).groups();

        if (Arrays.asList(groups).contains("Register user without delete account")) {
            userData = registerUserTest.registerUserWithLogoutAndGetUserData();
        }
    }

    private void loginProcess() {
        emailInput = userData.getEmail();
        passwordInput = userData.getPassword();
        String nameInput = userData.getName();

        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickSignupLogin();

        loginPage.verifyHeaderBeforeLoginIsVisibleSuccessfully()
                .setLoginEmailInput(emailInput)
                .setLoginPasswordInput(passwordInput)
                .clickLoginButton()
                .verifyLoggedUsernameIsVisibleSuccessfully(nameInput);
    }

    @BeforeMethod
    public void setupBeforeMethod(Method method) {
        faker = new Faker();
        appMainMethods = new AppMainMethods();
        homePage = new HomePage();
        loginPage = new LoginPage();
        registerUserTest = new RegisterUserTest();

        registerUserTest.initializeDependenciesAndVariables();

        performGroupSpecificSetup(method);
    }

    @Test(description = "Login User with correct email and password", groups = "Register user without delete account")
    public void loginUserWithCorrectEmailAndPassword() {
        loginProcess();

        homePage.clickDeleteAccountLink()
                .verifyHeaderTextAccountDeletedIsVisibleSuccessfully();
    }

    @Test(description = "Login User with incorrect email and password")
    public void loginUserWithIncorrectEmailAndPassword() {
        emailInput = faker.internet().emailAddress();
        passwordInput = faker.internet().password(MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH);

        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickSignupLogin();

        loginPage.verifyHeaderBeforeLoginIsVisibleSuccessfully()
                .setLoginEmailInput(emailInput)
                .setLoginPasswordInput(passwordInput)
                .clickLoginButton()
                .verifyErrorForIncorrectCredentialsIsVisibleSuccessfully();
    }

    @Test(description = "Logout User", groups = "Register user without delete account")
    public void logoutUserCorrectly() {
        loginProcess();

        homePage.clickLogoutLink();

        appMainMethods.verifyThatLoginPageIsVisibleSuccessfully();
    }
}