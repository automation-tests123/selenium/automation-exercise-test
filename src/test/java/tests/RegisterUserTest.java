package tests;

import com.github.javafaker.Faker;
import helpers.UserData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.SignupPage;
import utils.AppMainMethods;
import utils.TestSetup;


public class RegisterUserTest extends TestSetup {

    private static final int MIN_PASSWORD_LENGTH = 8;
    private static final int MAX_PASSWORD_LENGTH = 12;
    protected String nameInput;
    protected String emailInput;
    protected String passwordInput;
    protected String firstnameInput;
    protected String lastnameInput;
    protected String companyInput;
    protected String firstAddressInput;
    protected String secondAddressInput;
    protected String stateInput;
    protected String cityInput;
    protected String zipcodeInput;
    protected String mobileNumberInput;
    private Faker faker;
    private AppMainMethods appMainMethods;
    private HomePage homePage;
    private SignupPage signupPage;

    protected void initializeFakerVariables() {
        nameInput = faker.name().username();
        emailInput = faker.internet().emailAddress();
        passwordInput = faker.internet().password(MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH);
        firstnameInput = faker.name().firstName();
        lastnameInput = faker.name().lastName();
        companyInput = faker.company().name();
        firstAddressInput = faker.address().streetAddress();
        secondAddressInput = faker.address().buildingNumber();
        stateInput = faker.address().state();
        cityInput = faker.address().city();
        zipcodeInput = faker.address().zipCode();
        mobileNumberInput = faker.phoneNumber().cellPhone();
    }

    protected void initializeDependencies() {
        faker = new Faker();
        appMainMethods = new AppMainMethods();
        homePage = new HomePage();
        signupPage = new SignupPage();
    }

    protected void initializeDependenciesAndVariables() {
        initializeDependencies();

        initializeFakerVariables();
    }

    protected void registerUserWithoutDeleteAccountAndLogout() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickSignupLogin();

        signupPage.verifyHeaderBeforeRegistrationIsVisibleSuccessfully()
                .setSignupNameInput(nameInput)
                .setSignupEmailInput(emailInput)
                .clickSignupButton()
                .verifyHeaderDuringRegistrationIsVisibleSuccessfully()
                .chooseSignupTitleCheckbox()
                .setSignupPasswordInput(passwordInput)
                .chooseSignupRandomDayInSelect()
                .chooseSignupRandomMonthInSelect()
                .chooseSignupRandomYearInSelect()
                .clickSignupNewsletterCheckbox()
                .clickSignupSpecialOffersCheckbox()
                .setSignupFirstnameInput(firstnameInput)
                .setSignupLastnameInput(lastnameInput)
                .setSignupCompanyInput(companyInput)
                .setSignupFirstAddressInput(firstAddressInput)
                .setSignupSecondAddressInput(secondAddressInput)
                .chooseSignupRandomCountryInSelect()
                .setSignupStateInput(stateInput)
                .setSignupCityInput(cityInput)
                .setSignupZipcodeInput(zipcodeInput)
                .setSignupMobileNumberInput(mobileNumberInput)
                .clickSignupCreateAccountButton()
                .verifyHeaderAfterRegistrationIsVisibleSuccessfully()
                .clickSignupContinueButton();

        signupPage.verifyLoggedInTextUsernameIsVisibleSuccessfully(nameInput);

        homePage.clickLogoutLink()
                .clickHomeLink();
    }

    protected void registerUserWithoutDeleteAccountByCheckout() {
        signupPage.verifyHeaderBeforeRegistrationIsVisibleSuccessfully()
                .setSignupNameInput(nameInput)
                .setSignupEmailInput(emailInput)
                .clickSignupButton()
                .verifyHeaderDuringRegistrationIsVisibleSuccessfully()
                .chooseSignupTitleCheckbox()
                .setSignupPasswordInput(passwordInput)
                .chooseSignupRandomDayInSelect()
                .chooseSignupRandomMonthInSelect()
                .chooseSignupRandomYearInSelect()
                .clickSignupNewsletterCheckbox()
                .clickSignupSpecialOffersCheckbox()
                .setSignupFirstnameInput(firstnameInput)
                .setSignupLastnameInput(lastnameInput)
                .setSignupCompanyInput(companyInput)
                .setSignupFirstAddressInput(firstAddressInput)
                .setSignupSecondAddressInput(secondAddressInput)
                .chooseSignupRandomCountryInSelect()
                .setSignupStateInput(stateInput)
                .setSignupCityInput(cityInput)
                .setSignupZipcodeInput(zipcodeInput)
                .setSignupMobileNumberInput(mobileNumberInput)
                .clickSignupCreateAccountButton()
                .verifyHeaderAfterRegistrationIsVisibleSuccessfully()
                .clickSignupContinueButton();

        signupPage.verifyLoggedInTextUsernameIsVisibleSuccessfully(nameInput);
    }

    public UserData registerUserWithLogoutAndGetUserData() {
        registerUserWithoutDeleteAccountAndLogout();

        return new UserData.Builder()
                .setEmail(emailInput)
                .setPassword(passwordInput)
                .setName(nameInput)
                .build();
    }

    public UserData registerUserWithoutDeleteAccountAndGetUserData() {
        registerUserWithoutDeleteAccountByCheckout();

        return new UserData.Builder()
                .setEmail(emailInput)
                .setPassword(passwordInput)
                .setName(nameInput)
                .setFirstName(firstnameInput)
                .setLastName(lastnameInput)
                .setFirstAddressInput(firstAddressInput)
                .setSecondAddressInput(secondAddressInput)
                .setStateInput(stateInput)
                .setCityInput(cityInput)
                .setZipcodeInput(zipcodeInput)
                .setMobileNumberInput(mobileNumberInput)
                .build();
    }

    @BeforeMethod
    public void setupBeforeMethod() {
        initializeDependencies();
    }

    @Test(description = "Register user correctly")
    public void registerUserCorrectly() {
        initializeFakerVariables();

        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickSignupLogin();

        signupPage.verifyHeaderBeforeRegistrationIsVisibleSuccessfully()
                .setSignupNameInput(nameInput)
                .setSignupEmailInput(emailInput)
                .clickSignupButton()
                .verifyHeaderDuringRegistrationIsVisibleSuccessfully()
                .chooseSignupTitleCheckbox()
                .setSignupPasswordInput(passwordInput)
                .chooseSignupRandomDayInSelect()
                .chooseSignupRandomMonthInSelect()
                .chooseSignupRandomYearInSelect()
                .clickSignupNewsletterCheckbox()
                .clickSignupSpecialOffersCheckbox()
                .setSignupFirstnameInput(firstnameInput)
                .setSignupLastnameInput(lastnameInput)
                .setSignupCompanyInput(companyInput)
                .setSignupFirstAddressInput(firstAddressInput)
                .setSignupSecondAddressInput(secondAddressInput)
                .chooseSignupRandomCountryInSelect()
                .setSignupStateInput(stateInput)
                .setSignupCityInput(cityInput)
                .setSignupZipcodeInput(zipcodeInput)
                .setSignupMobileNumberInput(mobileNumberInput)
                .clickSignupCreateAccountButton()
                .verifyHeaderAfterRegistrationIsVisibleSuccessfully()
                .clickSignupContinueButton();

        signupPage.verifyLoggedInTextUsernameIsVisibleSuccessfully(nameInput);

        homePage.clickDeleteAccountLink()
                .verifyHeaderTextAccountDeletedIsVisibleSuccessfully();

        signupPage.clickSignupContinueButton();
    }

    @Test(description = "Register User with existing email")
    public void registerUserWithExistingEmailIncorrectly() {
        initializeFakerVariables();

        registerUserWithoutDeleteAccountAndLogout();

        homePage.clickSignupLogin();

        signupPage.verifyHeaderBeforeRegistrationIsVisibleSuccessfully()
                .setSignupNameInput(nameInput)
                .setSignupEmailInput(emailInput)
                .clickSignupButton()
                .verifyErrorForEmailAlreadyExistIsVisibleSuccessfully();
    }
}
