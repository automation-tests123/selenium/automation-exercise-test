package tests;

import com.github.javafaker.Faker;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.ProductsPage;
import utils.AppMainMethods;
import utils.TestSetup;


public class ProductsTest extends TestSetup {

    private static final int REVIEW_WORD_COUNT = 64;
    private AppMainMethods appMainMethods;
    private HomePage homePage;
    private ProductsPage productsPage;
    private Faker faker;

    private void navigateAndVerifyProductsPage() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickProductsLink();

        productsPage.verifyThatProductsPageIsVisibleSuccessfully()
                .verifyHeaderAllProductsIsVisibleSuccessfully()
                .verifyAllProductsAreVisibleSuccessfully();
    }

    @BeforeMethod
    public void setupBeforeMethod() {
        appMainMethods = new AppMainMethods();
        homePage = new HomePage();
        productsPage = new ProductsPage();
        faker = new Faker();
    }

    @Test(description = "Verify test cases page")
    public void verifyAllProductsAndProductDetailPageSuccessfully() {
        navigateAndVerifyProductsPage();

        productsPage.clickOnFirstViewProduct()
                .verifyThatProductDetailsPageIsVisibleSuccessfully()
                .verifyThatDetailsOfFirstProductAreVisibleSuccessfully();
    }

    @Test(description = "Search Product")
    @Parameters({"searchedProduct", "numOfSearchedProducts"})
    public void searchProductCorrectly(String searchedProduct, int numOfSearchedProducts) {
        navigateAndVerifyProductsPage();

        productsPage.setSearchProductEngineInput(searchedProduct)
                .clickSearchProductButton()
                .verifyHeaderSearchedProductsIsVisibleSuccessfully()
                .verifyAllSearchedProductsAreVisibleSuccessfully(numOfSearchedProducts)
                .verifyNameOfProductInPreviewIsSameAsSearchedProdcutSuccessfully(searchedProduct);
    }

    @Test(description = "View Category Products")
    public void viewCategoryProducts() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.verifyThatCategoriesOnLeftSideBarAreVisibleSuccessfully()
                .expandWomenCategory()
                .clickFirstWomenSubCategoryInLeftSideBar();

        productsPage.verifyFirstWomenSubCategoryHeaderIsVisibleSuccessfully();

        homePage.expandMenCategory()
                .clickFirstMenSubCategoryInLeftSideBar();

        productsPage.verifyFirstMenSubCategoryHeaderIsVisibleSuccessfully();
    }

    @Test(description = "View & Cart Brand Products")
    public void viewAndCartBrandProducts() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickProductsLink();

        productsPage.verifyThatBrandsOnLeftSideBarAreVisibleSuccessfully()
                .clickRandomBrand()
                .verifyHeaderContainsBrandIsVisibleSuccessfully()
                .verifyThatProducstInBrandAreVisibleSuccessfully()
                .clickRandomBrand()
                .verifyHeaderContainsBrandIsVisibleSuccessfully()
                .verifyThatProducstInBrandAreVisibleSuccessfully();
    }

    @Test(description = "Add review on product")
    public void addReviewOnProduct() {
        appMainMethods.verifyThatHomePageIsVisibleSuccessfully();

        homePage.clickProductsLink();

        String name = faker.name().name();
        String email = faker.internet().emailAddress();
        String review = faker.lorem().sentence(REVIEW_WORD_COUNT);

        productsPage.verifyThatProductsPageIsVisibleSuccessfully()
                .verifyHeaderAllProductsIsVisibleSuccessfully()
                .clickFirstViewProduct()
                .verifyWriteYourReviewIsVisibleSuccessfully()
                .setNameFormInput(name)
                .setEmailFormInput(email)
                .setReviewFormTextarea(review)
                .clickSubmitFormButton()
                .verifySuccessMessageForReviewSuccessfully();
    }
}